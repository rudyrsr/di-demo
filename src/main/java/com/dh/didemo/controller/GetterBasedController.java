package com.dh.didemo.controller;

import com.dh.didemo.service.GreetingService;
import org.springframework.stereotype.Controller;

@Controller
public class GetterBasedController {
    private GreetingService greetingService;
    public String sayHello(){
        return greetingService.sayGreeting();

    }

    public void setGreetingService(GreetingService greetingService) {
        this.greetingService = greetingService;
    }
}
