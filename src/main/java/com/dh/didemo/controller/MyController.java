package com.dh.didemo.controller;

import org.springframework.context.annotation.Configuration;

@Configuration
public class MyController {
    public String hello(){
        String greeting = "hello Spring";
        System.out.println(greeting);
        return greeting;

    }
}
