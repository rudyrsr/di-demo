package com.dh.didemo.controller;

import com.dh.didemo.service.GreetingService;
import org.springframework.stereotype.Controller;

@Controller
public class ContructorBasedController {
    private GreetingService greetingService;

    public ContructorBasedController(GreetingService greetingService) {
        this.greetingService = greetingService;
    }

    public String sayHello(){
        return greetingService.sayGreeting();

    }
}
