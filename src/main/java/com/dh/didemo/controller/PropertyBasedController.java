package com.dh.didemo.controller;

import com.dh.didemo.service.GreetingService;
import org.springframework.stereotype.Controller;

@Controller
public class PropertyBasedController {
 public GreetingService greetingService;
 public String sayHello(){
     return greetingService.sayGreeting();

 }
}

