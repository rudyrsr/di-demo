package com.dh.didemo.controller;

import com.dh.didemo.service.GreetingService;
import com.dh.didemo.service.GreetingServiceImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLOutput;

import static org.junit.Assert.*;

public class ContructorBasedControllerTest {
    private ContructorBasedController contructorBasedController;
    @Before
    public void setUp() throws Exception {
        GreetingService greetingService= new GreetingServiceImpl();
        contructorBasedController=new ContructorBasedController(greetingService);
        System.out.println("@Before");
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("@After");
    }

    @Test
    public void sayHello() {
        System.out.println("@Test setHello");
        String greeting=contructorBasedController.sayHello();
        Assert.assertEquals(GreetingServiceImpl.GREETING,greeting);

    }
}