package com.dh.didemo.controller;
import com.dh.didemo.service.GreetingServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import sun.plugin.util.ProgressMonitor;

import static org.junit.Assert.*;

public class PropertyBasedControllerTest {
    private PropertyBasedController  propertyBasedController;
    @Before
    public void before(){
        propertyBasedController=new PropertyBasedController();
        propertyBasedController.greetingService=new GreetingServiceImpl();

    }
    @Test
    public void sayHello() {
        String greeting=propertyBasedController.sayHello();
        Assert.assertEquals(GreetingServiceImpl.GREETING,greeting);

    }
}