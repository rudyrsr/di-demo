package com.dh.didemo.controller;

import com.dh.didemo.service.GreetingServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class GetterBasedControllerTest {
    private GetterBasedController getterBasedController;

    @Before
    public void Before() throws Exception {
        getterBasedController=new GetterBasedController();
       getterBasedController.setGreetingService(new GreetingServiceImpl());
    }

    @Test
    public void sayHello() {
        String greeting=getterBasedController.sayHello();
        Assert.assertEquals(GreetingServiceImpl.GREETING,greeting);

    }
}